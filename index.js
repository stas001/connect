const connect = require('connect')();
const logger = require('./logger');
const errorHandler = require('./error-handler');

function sayHello(req, res, next) {
    res.end('Hello');
}

connect.use(logger(':method :url'))
    .use(sayHello)
    .use(errorHandler)
    .listen(3000);